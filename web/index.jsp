<%@page import="org.mypackage.varatutsanat.Peli"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Varatut Sanat</title>
    </head>
    <body>

        <%
            Peli peli = new Peli();
        %>
        <h1>Varatut Sanat</h1>
        <p>Muistatko javan varatut sanat?</p>

        <p>Tämä versio sisältää varatuista sanoista seuraavat kategoriat: </p>

        <ul>
            <%                for (String kategoria : peli.getKategorioidenNimet()) {%>

            <li><%= kategoria.substring(0, 1).toUpperCase() + kategoria.substring(1)%></li>

            <%}%>
        </ul>
        <p>Sanoja pelissä on yhteensä <%= peli.annaSanojenMaara()%></p>
        <p>Voit valita aikapelin, jos haluat kokeilla, muistatko kaikki sanat tietyssä ajassa</p>

        <form action="start.jsp" method="get">
            <input type="checkbox" id="aikapeli" name="aikapeli"> <label>Aikapeli</label>
            <input type="submit" value="Aloita">
        </form>
    </body>
</html>
