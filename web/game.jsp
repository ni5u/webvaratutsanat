<%-- 
    Document   : game
    Created on : 6.12.2017, 17:58:23
    Author     : nisu
--%>
<%@page import="org.mypackage.varatutsanat.Aikapeli"%>
<%@page import="org.mypackage.varatutsanat.Peli"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Varatut sanat</title>
    </head>
    <body>
        <h1>Varatut sanat</h1>
        <%
            Peli peli = (Peli) session.getAttribute("peli");
            String status = "";
            request.setCharacterEncoding("UTF-8");
            int katNo = Integer.parseInt(request.getParameter("katNo"));
            String nykyinenKategoria = request.getParameter("kategoria");
            String sana = request.getParameter("sana");
            if (peli instanceof Aikapeli) {
                if (request.getMethod() == "GET") {
                    ((Aikapeli) peli).kaynnistaAjanotto();
                }
            }
            if (peli.onkoSanaListassa(sana, nykyinenKategoria) > 0) {
                status = "oikein";
                if (peli.getKategorianPisteet(nykyinenKategoria) == peli.getKategorianMaksimipisteet(nykyinenKategoria)) {
                    katNo++;
                    status = "kategoria";
                    if (peli instanceof Aikapeli) {
                        if (((Aikapeli) peli).annaErotus() < 0) {
                            status = "gameover";
                        }
                    }
                }
            } else {
                status = "gameover";
            }
            if (katNo > 5) {
                status = "voitto";
            }

            if (status.equals("oikein") || status.equals("kategoria")) {%>

        <p>Oikein meni!</p>
        <p>Sanoja jäljellä koko pelissä: <%= peli.annaSanojenMaara() - peli.getMuistettujenSanojenLkm()%></p>
        <p><strong><%=nykyinenKategoria.substring(0, 1).toUpperCase() + nykyinenKategoria.substring(1)%>:</strong>
            <%= peli.annaKategorianArvatutSanat(nykyinenKategoria)%> / <%= peli.annaSanojenMaaraKategoria(nykyinenKategoria)%> sanaa muistettu.</p>
            <% } %>

        <% switch (katNo) {
                case 1:
                    nykyinenKategoria = "alkeistietotyypit";
                    break;
                case 2:
                    nykyinenKategoria = "muuntimet";
                    break;
                case 3:
                    nykyinenKategoria = "näkyvyysmääreet";
                    break;
                case 4:
                    nykyinenKategoria = "toistolauseet";
                    break;
                case 5:
                    nykyinenKategoria = "valintalauseet";
                    break;
                default:
                    break;
            }
            if (status.equals("kategoria")) {%>
        <p><strong>Seuraava kategoria: </strong><%=nykyinenKategoria.substring(0, 1).toUpperCase() + nykyinenKategoria.substring(1)%></p>
        <%
            if (peli instanceof Aikapeli) {%>
        <p>Aikaa jäljellä <%=((Aikapeli) peli).annaErotus()%> sekuntia.</p>
        <% }
            }
            if (status.equals("oikein") || status.equals("kategoria")) {%>
        <form method="post" action="game.jsp">
            <label>Sana: </label>
            <input type="text" name="sana" autofocus="autofocus" />
            <input type="hidden" name="kategoria" value="<%=nykyinenKategoria%>" />
            <input type="hidden" name="katNo" value="<%=katNo%>" />
            <button>Hyväksy</button>
        </form>
        <% } %>
        <% if (status.equals("gameover")) {
        %>
        <p>VOI RÄHMÄ!</p>
        <% if (peli instanceof Aikapeli && ((Aikapeli) peli).annaErotus() < 0) {%>
        <p>Aika loppui. Sitä ehti kulua <%= ((Aikapeli) peli).annaKulunutAika()%> </p>
        <% }
            } %>
        <% if (status.equals("voitto")) { %>
        <h1>Onneksi olkoon!</h1>
        <p>Tiesit kaikki sanat!</p>
        <% if (peli instanceof Aikapeli) {%>
        <p>Aikaa kului <%= ((Aikapeli) peli).annaKulunutAika()%> sekuntia.</p>
        <% }
            } %>
        <table>
            <tr>
                <th>Kategoria</th>
                <th>Pisteet</th>
                <th>Maksimi</th>
            </tr>

            <%    for (String kategoria : peli.getKategorioidenNimet()) {%>

            <tr><td><%= kategoria.substring(0, 1).toUpperCase() + kategoria.substring(1)%></td>    
                <td><%=peli.getKategorianPisteet(kategoria)%></td>
                <td><%=peli.getKategorianMaksimipisteet(kategoria)%></td>
            </tr>

            <%}%>
            <tr><th>Yhteensä</th>
                <td><%=peli.getPisteet()%></td>
                <td><%=peli.getMaksimipisteet()%></td>
            </tr>
        </table>
        <% if (status.equals("gameover") || status.equals("voitto")) { %>
        <h2>Uusi Peli</h2>
        <p>Voit valita aikapelin, jos haluat kokeilla muistatko kaikki sanat tietyssä ajassa.</p>
        <form action="start.jsp" method="get">
            <input type="checkbox" id="aikapeli" name="aikapeli"> <label>Aikapeli</label><br>
            <input type="submit" value="Aloita">
        </form>
        <% }%>

    </body>
</html>
