<%@page import="org.mypackage.varatutsanat.Aikapeli"%>
<%@page import="org.mypackage.varatutsanat.Peli"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Varatut Sanat</title>
    </head>
    <body>
        <h1>Varatut Sanat</h1>

        <%
            Peli peli = new Peli();

            if (request.getParameter("aikapeli") != null) { %>

        <p>Valitsit aikapelin. Aikaa on 60 sekuntia. </p> 
        <p>Näet jäljellä olevan ajan jokaisen kategorian jälkeen.</p>
        <p>Ajanotto alkaa kun aloitat pelin...</p>
        <%       peli = new Aikapeli();            
        } else {%>
        <p>Valitsit normaalin pelin ilman ajanottoa</p>
        <% } %>
        
        <p><strong>Kategoria: </strong>Alkeistietotyypit</p>

        <form method="get" action="game.jsp">
            <label>Sana: </label>
            <input type="text" name="sana" autofocus="autofocus" />
            <input type="hidden" name="kategoria" value="alkeistietotyypit" />
            <input type="hidden" name="katNo" value="1" />
            <button>Hyväksy</button>
        </form>
        <table>
            <tr>
                <th>Kategoria</th>
                <th>Pisteet</th>
                <th>Maksimi</th>
            </tr>
            <%                for (String kategoria : peli.getKategorioidenNimet()) {%>

            <tr><td><%= kategoria.substring(0, 1).toUpperCase() + kategoria.substring(1) %></td>    
                <td><%=peli.getKategorianPisteet(kategoria)%></td>
                <td><%=peli.getKategorianMaksimipisteet(kategoria)%></td>
            </tr>

            <%}%>
            <tr>
                <th>Yhteensä</th>
                <td><%=peli.getPisteet()%></td>
                <td><%=peli.getMaksimipisteet()%></td>
            </tr>
        </table>
        <% session.setAttribute("peli", peli);%>
    </body>
</html>