/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mypackage.varatutsanat;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Set;

/**
 *
 * @author nisu
 */
public class Peli {

    private TreeMap<String, ArrayList<Sana>> kategoriat = new TreeMap();

    // Pelaajan keräämät pisteet pelissä muistettujen sanojen perusteella
    private TreeMap<String, Integer> pisteet = new TreeMap();

    private int muistettujenSanojenLkm = 0;
    

    public Peli() {

        Alkeistietotyypit();
        Nakyvyysmaareet();
        Valintalauseet();
        Toistolauseet();
        Muuntimet();
    }

    public int getMuistettujenSanojenLkm() {
        return muistettujenSanojenLkm;
    }

    public int getMaksimipisteet() {
        int maksimipisteet = 0;
        for (String kategoria : kategoriat.keySet()) {

            for (Sana sana : kategoriat.get(kategoria)) {
                maksimipisteet += sana.getPisteet();
            }

        }
        return maksimipisteet;
    }

    public ArrayList<String> getKategorioidenNimet() {
        ArrayList<String> kategorioidenNimet = new ArrayList();

        for (String kategoria : kategoriat.keySet()) {
            kategorioidenNimet.add(kategoria);
        }

        return kategorioidenNimet;
    }

    public int annaKategorianArvatutSanat(String kategoria) {
        int arvatutSanat = 0;
        for(Sana sana : kategoriat.get(kategoria)) {
            if(sana.isArvattu()) {
                arvatutSanat++;
            }            
        }
        return arvatutSanat;
    }
    
    public int annaSanojenMaaraKategoria(String kategoria) {
        return kategoriat.get(kategoria).size();
    }
    
    public int annaSanojenMaara() {
        int sanoja = 0;

        for (String kategoria : kategoriat.keySet()) {
            sanoja += kategoriat.get(kategoria).size();
        }
        return sanoja;
    }
    
    public int getPisteet() {
        int yhteispisteet = 0;
        for (String kategoria : pisteet.keySet()) {
            yhteispisteet += pisteet.get(kategoria);
        }
        return yhteispisteet;
    }

    public int getKategorianPisteet(String kategoria) {

        return pisteet.get(kategoria);
    }

    public void lisaaPisteet(String kategoria, int sanapisteet) {
        pisteet.put(kategoria, pisteet.get(kategoria) + sanapisteet);

    }

    public int getKategorianMaksimipisteet(String kategoria) {
        int maksimipisteet = 0;
        for (Sana sana : kategoriat.get(kategoria)) {
            maksimipisteet += sana.getPisteet();
        }
        return maksimipisteet;
    }

    // Metodi, joko tarkistaa onko parametrina välitetty teksti listassa ja 
    // palauttaa joko sanapisteet tai arvon 0
    public int onkoSanaListassa(String teksti, String kategoria) {

        // Käydään toistorakenteessa läpi sanaolioita sisältävä lista
        for (Sana sana : kategoriat.get(kategoria)) {

            // Verrataan listassa olevaa sanaa parametrina välitettyyn sanaan
            // ja tarkistetaan onko sana arvattu aiemmin
            if (sana.getTeksti().equals(teksti) && (sana.isArvattu() == false)) {

                // Sana löytyi listasta eikä se ollut aiemmin arvattu sana.
                // Merkitään se arvatuksi, lasketaan yhteispisteet ja 
                // palautetaan kutsuvaan ohjelmaan sanapisteet
                this.muistettujenSanojenLkm++;
                sana.setArvattu(true);
                lisaaPisteet(kategoria, sana.getPisteet());
                return sana.getPisteet();
            }

        } // for

        // Jos tänne saakka päästään, ei pisteitä anneta
        return 0;

    } // onkoSanaListassa

    private void Alkeistietotyypit() {

        pisteet.put("alkeistietotyypit", 0);
        ArrayList<Sana> alkeistietotyypit = new ArrayList();
        alkeistietotyypit.add(new Alkeistietotyyppi("boolean"));
        alkeistietotyypit.add(new Alkeistietotyyppi("short"));
        alkeistietotyypit.add(new Alkeistietotyyppi("long"));
        alkeistietotyypit.add(new Alkeistietotyyppi("byte"));
        alkeistietotyypit.add(new Alkeistietotyyppi("char"));
        alkeistietotyypit.add(new Alkeistietotyyppi("float"));
        alkeistietotyypit.add(new Alkeistietotyyppi("double"));
        alkeistietotyypit.add(new Alkeistietotyyppi("int")); 
        kategoriat.put("alkeistietotyypit", alkeistietotyypit);
    }

    private void Nakyvyysmaareet() {

        pisteet.put("näkyvyysmääreet", 0);
        ArrayList<Sana> nakyvyysmaareet = new ArrayList();
        nakyvyysmaareet.add(new Nakyvyysmaare("private"));
        nakyvyysmaareet.add(new Nakyvyysmaare("protected"));
        nakyvyysmaareet.add(new Nakyvyysmaare("public"));        
        kategoriat.put("näkyvyysmääreet", nakyvyysmaareet);
    }

    private void Toistolauseet() {

        pisteet.put("toistolauseet", 0);
        ArrayList<Sana> toistolauseet = new ArrayList();
        toistolauseet.add(new Toistolause("for"));
        toistolauseet.add(new Toistolause("foreach"));
        toistolauseet.add(new Toistolause("do"));
        toistolauseet.add(new Toistolause("while"));
        kategoriat.put("toistolauseet", toistolauseet);
    }

    private void Valintalauseet() {
        
        pisteet.put("valintalauseet", 0);
        ArrayList<Sana> valintalauseet = new ArrayList();
        valintalauseet.add(new Valintalause("if"));
        valintalauseet.add(new Valintalause("else if"));
        valintalauseet.add(new Valintalause("else"));
        valintalauseet.add(new Valintalause("switch"));
        valintalauseet.add(new Valintalause("case"));
        kategoriat.put("valintalauseet", valintalauseet);

    }

    private void Muuntimet() {

        pisteet.put("muuntimet", 0);
        ArrayList<Sana> muuntimet = new ArrayList();
        muuntimet.add(new Muunnin("final"));
        muuntimet.add(new Muunnin("static"));
        muuntimet.add(new Muunnin("abstract"));
        muuntimet.add(new Muunnin("interface"));
        kategoriat.put("muuntimet", muuntimet);
    }

}
