/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mypackage.varatutsanat;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author nisu
 */
public class Aikapeli extends Peli{
    
    private long tavoiteaika_ms = 60000;
    private long aloitusaika_ms;
    private long kulunutaika_ms;
    
    public Aikapeli() {
        super();
    }
    
    public long annaTavoiteaika() {
        
        return TimeUnit.MILLISECONDS.toSeconds(tavoiteaika_ms);
    }
    
    public void kaynnistaAjanotto() {
        
        aloitusaika_ms = System.currentTimeMillis();
    }
    
    public long annaErotus(){
        
        return annaTavoiteaika() - annaKulunutAika();
    }
    
    private void laskeKulunutAika() {
        
        kulunutaika_ms = System.currentTimeMillis() - aloitusaika_ms;
    }
    
    public long annaKulunutAika() {
        
        laskeKulunutAika();
        return TimeUnit.MILLISECONDS.toSeconds(kulunutaika_ms);
    }
    
    public boolean peliJatkuu() {
        
        laskeKulunutAika();
        return (kulunutaika_ms < tavoiteaika_ms);
    }
    
    
}
