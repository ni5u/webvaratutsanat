package org.mypackage.varatutsanat;

/**
 *
 * @author nisu
 */
public class OmaVaratutSanat {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Peli peli = new Peli();
        UI ui = new UI();

        ui.tulostaOtsikko(peli.annaSanojenMaara(), peli.getKategorioidenNimet());

        if (ui.pelaaAikapeli()) {
            peli = new Aikapeli();
        }

        if (peli instanceof Aikapeli) {
            ui.tulostaAikapelinTiedot(((Aikapeli) peli).annaTavoiteaika());
            ((Aikapeli) peli).kaynnistaAjanotto();
        }

        for (String kategoria : peli.getKategorioidenNimet()) {
            if (peli instanceof Aikapeli) {

                ui.tulostaJaljellaOlevaAika(((Aikapeli) peli).annaErotus());
                if (!((Aikapeli) peli).peliJatkuu()) {
                    break;
                }
            }
            boolean jatkuu = kasitteleKategoria(peli, ui, kategoria);
            if (!jatkuu) {
                break;
            }
        }
        if (peli instanceof Aikapeli) {
            ui.tulostaLoppuTervehdysAikapeli(peli.annaSanojenMaara() - peli.getMuistettujenSanojenLkm(), ((Aikapeli) peli).annaTavoiteaika(), ((Aikapeli) peli).annaKulunutAika());
        } else {
            ui.tulostaLoppuTervehdys(peli.annaSanojenMaara() - peli.getMuistettujenSanojenLkm());
        }

    }

    private static boolean kasitteleKategoria(Peli peli, UI ui, String kategoria) {

        while (peli.getKategorianPisteet(kategoria) < peli.getKategorianMaksimipisteet(kategoria)) {
            ui.tulostaKategoria(kategoria);
            int jatkuu = peli.getPisteet();
            ui.tulostaPelinTilanne(peli.onkoSanaListassa(ui.kysySana(), kategoria), peli.getPisteet(), peli.getMaksimipisteet(),
                    peli.annaSanojenMaara() - peli.getMuistettujenSanojenLkm(), kategoria, peli.getKategorianPisteet(kategoria),
                    peli.getKategorianMaksimipisteet(kategoria));
            if (jatkuu == peli.getPisteet()) {
                return false;
            }
        }
        return true;
    }
}
