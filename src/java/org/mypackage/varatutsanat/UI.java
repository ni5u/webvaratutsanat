/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mypackage.varatutsanat;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Nisu
 */
public class UI {

    // Jäsenmuuttuja syötteiden lukemista varten
    private Scanner lukija = new Scanner(System.in);

    // Metodi, joka ottaa parametrina sanojen lukumäärän ja tulostaa otsikon
    public void tulostaOtsikko(int sanatLkm, ArrayList<String> kategoriat) {

        System.out.println("\nMUISTATKO JAVAN VARATUT SANAT?");
        System.out.println("\nTämä versio sisältää varatuista sanoista ");

        for (String kategoria : kategoriat) {
            System.out.println(kategoria);
        }
        System.out.println("Niitä on yhteensä " + sanatLkm + " sanaa.");

    } // tulostaOtsikko

    public boolean pelaaAikapeli() {

        System.out.println("Pelataanko aikapeli?");
        System.out.println("");
        System.out.println("E = Ei");
        System.out.println("K = Kyllä");
        System.out.println("");
        System.out.print("Vastaus: ");
        String vastaus = lukija.nextLine();

        return "K".equals(vastaus.toUpperCase());
    }

    public void tulostaAikapelinTiedot(long tavoiteaika) {
        System.out.println("Valitsit aikapelin. Aikaa on " + tavoiteaika + " sekuntia. Näet ajan jokaisen kategorian jälkeen.");
    }

    public void tulostaJaljellaOlevaAika(long aikaaJaljella) {
        if (aikaaJaljella > 0) {
            System.out.println("Aikaa jäljellä " + aikaaJaljella + " sekuntia...");
        } else {
            System.out.println("Aika loppui.");
        }

    }

    // Metodi, joka kysyy sanan ja palauttaa sen
    public String kysySana() {

        System.out.print("\nAnna sana: ");
        return lukija.nextLine();

    } // kysySana

    public void tulostaKategoria(String kategoria) {
        System.out.println(kategoria);
    }

    // Metodi, joka tulostaa pelin tilanteen parametrina välitettyjen tietojen
    // mukaan
    public void tulostaPelinTilanne(int sanapisteet, int yhteispisteet,
            int maksimipisteet, int sanojaJaljella, String kategoria, int kategorianPisteet, int kategorianMaksimipisteet) {

        if (sanapisteet > 1) {
            System.out.println("\nOIKEIN!");
        } else {
            System.out.println("\nAUTS! Syötit väärän sanan tai sanan, jonka olet jo syöttänyt. ");
        }

        System.out.println("Sanapisteet:\t\t" + sanapisteet);
        String kategoriaCap = kategoria.substring(0, 1).toUpperCase() + kategoria.substring(1);
        System.out.println(kategoriaCap);
        System.out.println("Kategorian pisteet:\t" + kategorianPisteet + " / " + kategorianMaksimipisteet);
        System.out.println("Yhteispisteet:\t\t" + yhteispisteet + " / " + maksimipisteet);

        System.out.println("Sanoja jäljellä:\t" + sanojaJaljella);
        System.out.println("");

    } // tulostaPelinTilanne

    // Metodi, joka tulostaa lopputervehdyksen ja parametrina välitetyn
    // arvon mukaan arvaamatta jääneiden sanojen lukumäärän
    public void tulostaLoppuTervehdys(int sanojaJaljella) {

        if (sanojaJaljella == 0) {
            System.out.println("\nErinomaista. Muistit kaikki sanat.\n");
        } else {
            System.out.println("\nSanoja jäi " + sanojaJaljella
                    + ". Vielä jäi vähän parannettavaa.\n");
        }

    } // tulostaLoppuTervehdys

    public void tulostaLoppuTervehdysAikapeli(int sanojaJaljella, long tavoiteaika, long aikaaKului) {

        if (sanojaJaljella > 0) {
            System.out.println("\nSanoja jäi " + sanojaJaljella
                    + ". Vielä jäi vähän parannettavaa.\n");
        } else {
            if (aikaaKului < tavoiteaika) {
                System.out.println("\nErinomaista. Muistit kaikki sanat tavoiteajassa.\n");                
            } else {
                System.out.println("Hyvä. Muistit kaikki sanat, mutta aika ei ihan riittänyt.");
            }          

        }

        System.out.println("Aikaa kului " + aikaaKului + " sekuntia...");
    }

} // UI (class)
