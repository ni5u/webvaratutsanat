/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mypackage.varatutsanat;

/**
 *
 * @author Nisu
 */
public class Sana {
    
    // Varattu sana
    private String teksti;              
    
    // Varatun sanan pistemäärä ts. eri sanoilla voi olla eri pistemäärä
    private int pisteet;
    
    // Onko tämä sana jo arvattu meneillään olevassa pelissä?
    private boolean arvattu;
    
    //Sanan kategoria
    private String kategoria; 
    
    // Alustaja, joka ottaa paramterina sanan ja pistemäärän sekä sijoittaa
    // ne luokan jäsenmuuttujiin
    public Sana(String teksti, int pisteet) {
        this.teksti = teksti;
        this.pisteet = pisteet;
    }

    // Metodi, joka asettaa parametrina välitetyn arvattu-totuusarvon luokan
    // jäsenmuuttujaan
    public void setArvattu(boolean arvattu) {
        this.arvattu = arvattu;
    }

    // Metodi, joka palauttaa luokan jäsenmuuttujassa olevan sanan
    public String getTeksti() {
        return teksti;
    }

    // Metodi, joka palauttaa luokan jäsenmuuttujassa olevan sanan pistemäärän
    public int getPisteet() {
        return pisteet;
    }

    // Metodi, joka palauttaa luokan jäsenmuuttujassa olevan arvattu-totuusarvon
    public boolean isArvattu() {
        return arvattu;
    }
    
       public String getKategoria() {
        return kategoria;
    }

} // Sana (class)